/**
 * 
 */
package br.com.etec;

/**
 * @author etecja
 *
 */
public class Calcular {
	
	public double calcularImc(double peso, double altura) {
		
		return peso / (altura * altura);
	}
}
