package tabuada;

import java.util.Scanner;

public class multiplicação {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		double valor;
		double vezes;
		double cont = 0;
		
		System.out.println("Informe qual tabuada que deseja: ");
		valor = scanner.nextDouble();
		
		System.out.println("Limite da tabuada: ");
		vezes = scanner.nextDouble();
		
		
		while (cont < vezes) {
			cont += 1;
			double res = (double)valor * cont;
 			System.out.println(valor + "X" + cont + "=" + res);
		}
	}

}
