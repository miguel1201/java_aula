package br.com.etec;

import java.util.Scanner;

public class Principal {
	
	/**
	 * 
	 * @param args
	 */
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		double peso;
		double altura;
		
		System.out.println("Qual o seu peso ?");
		peso = scanner.nextDouble();
		
		System.out.println("Qual a sua altura ?");
		altura = scanner.nextDouble();

		Calcular calcular = new Calcular();
		double res = calcular.calcularImc(peso, altura);
		System.out.println("Resultado: " + res);
		
		
		if (res < 20) {
			System.out.println("Voc� esta magro ");
			
		}else if (res >= 20 && res <= 24 ) {
			System.out.println("Voc� esta normal ");
			
		}else if (res >= 25 && res <= 29) {
			System.out.println("Voc� esta acima do peso ");
			
		}else if (res >= 30 && res <= 34) {
			System.out.println("Voc� esta obeso ");
		
		}else if (res > 34) {
			System.out.println("Voc� esta muito obeso ");
			
		}
	}
}
